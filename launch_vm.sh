#!/usr/bin/env bash
# Arguments : ipAddress macAddress vmName repoDirPath
set -e

. /grid5000/Modules/modules.sh
module load veertuosa
TAP=$(sudo create_tap)
veertuosa_launch -n $3 -i $4/veertuosa/images/g5k8s.qcow2 --ip-address $1 --mac-address $2 --tap $TAP