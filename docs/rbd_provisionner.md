## Deploy and configure RBD provisionner

### Automatically

```sh
rake storage:deploy
```

```sh
rake shell
root@kubemaster:~# kubectl get all --selector="app=rbd-provisioner"
NAME                                   READY     STATUS    RESTARTS   AGE
pod/rbd-provisioner-857866b5b7-ttmpd   1/1       Running   0          3m

NAME                              DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/rbd-provisioner   1         1         1            1           3m

NAME                                         DESIRED   CURRENT   READY     AGE
replicaset.apps/rbd-provisioner-857866b5b7   1         1         1         3m
```

### Manually

#### Deploy RBD provisionner

Get [external storage](https://github.com/kubernetes-incubator/external-storage) GIT repository from kubernetes-incubator :

```sh
rake shell
git clone https://github.com/kubernetes-incubator/external-storage.git
```

Deploy RBD external provisionner :

```sh
kubectl create -f external-storage/ceph/rbd/deploy/rbac/
```

#### Add secrets and dynamic storage class

Add secrets to connect to the Ceph cluster with your own Cephx key.

Create a `secrets.yaml` file :

```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: ceph-secret
  namespace: default
type: "kubernetes.io/rbd"
data:
  # ceph auth get-key client.$USER | base64
  key: xxx=
---
apiVersion: v1
kind: Secret
metadata:
  name: ceph-admin-secret
  namespace: default
type: "kubernetes.io/rbd"
data:
  # ceph auth get-key client.$USER | base64
  key: xxx=
```

!!! Note
    On the frontend, get your cephx key base64 encoded :

    ```sh
    ceph auth get-key client.$USER | base64
    ```

    Replace key value by the returned value for both `ceph-secret` and `ceph-admin-secret`.

Add secrets on kubemaster :

```sh
kubectl create -f secrets.yaml
```

!!! Tip
    On kubemaster (Veertuosa KVM virtual machine), your Grid'5000 home directory is available on `/mnt/home`.

    ```sh
    root@kubemaster:~# cat /etc/fstab
    #...
    home  /mnt/home  9p  trans=virtio,version=9p2000.L,nofail,x-systemd.device-timeout=10s 0 0
    grid5000  /mnt/grid5000  9p  trans=virtio,version=9p2000.L,nofail,x-systemd.device-timeout=10s 0 0
    tmp  /mnt/tmp  9p  trans=virtio,version=9p2000.L,nofail,x-systemd.device-timeout=10s 0 0
    ```

Create a `class.yaml` file :

```yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: rbd
provisioner: ceph.com/rbd
parameters:
  monitors: ceph0.site.grid5000.fr:6789
  pool: g5kUserId_g5k8s
  adminId: g5kUserId
  adminSecretNamespace: default
  adminSecretName: ceph-admin-secret
  userId: g5kUserId
  userSecretNamespace: default
  userSecretName: ceph-secret
  imageFormat: "2"
```

!!! Note
    * Replace `g5kUserId` by your Grid'5000 login for `pool`, `adminId` and `userId` parameters.
    * Replace `site` by the Grid'5000 site on `monitors` parameter.

Add the `rbd` storage class :

```sh
kubectl create -f class.yaml
```

## Clean RBDs created by a previous deployment

On the frontend :

```sh
for i in $(rbd -p ${USER}_g5k8s ls); do echo "--> Remove RBD $i"; rbd rm ${USER}_g5k8s/$i; done
--> Remove RBD kubernetes-dynamic-pvc-01d31213-9bd1-11e8-9afd-0a580af40204
Removing image: 100% complete...done.
--> Remove RBD kubernetes-dynamic-pvc-9be0b12b-9bd0-11e8-9afd-0a580af40204
Removing image: 100% complete...done.
--> Remove RBD kubernetes-dynamic-pvc-9bff2600-9bd0-11e8-9afd-0a580af40204
Removing image: 100% complete...done.
--> Remove RBD kubernetes-dynamic-pvc-eb8568dd-9bd0-11e8-9afd-0a580af40204
Removing image: 100% complete...done.
--> Remove RBD kubernetes-dynamic-pvc-f476692a-9bd0-11e8-9afd-0a580af40204
Removing image: 100% complete...done.
```