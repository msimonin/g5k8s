* OAR resources : nodes (>=3) + subnets.
* Veertuosa (Qemu KVM wrapper, only availble at Rennes and Nantes).
* Ceph for dynamic persistent volumes (Optional, only available at Rennes and Nantes).

## Initial setup

### Download Gitlab project

From a Grid'5000 frontend :

```sh
ssh frennes.rennes.grid5000.fr
git clone https://gitlab.inria.fr/pmorillo/g5k8s.git
cd g5k8s
```

### Ruby environment

```sh
source setup_env.sh
gem install bundler
bundle install
```

### Build qemu KVM image (~3mn)

```sh
oarsub -S ./veertuosa/generate_images.sh
```

This OAR job will create a virtual image based on Ubuntu 18.04 with :

* Last Kubernetes version and pre-pulled k8s containers.
* [Helm](http://helm.sh).

At the end of this OAR job, the image will be available at `./veertuosa/images/g5k8s.qcow2`.


!!! tip
    Remove this qcow2 file to generate a new one with latest Kubernetes version, and again :

    ```
    oarsub -S ./veertuosa/generate_image.sh
    ```

