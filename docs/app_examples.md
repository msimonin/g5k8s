## Elasticsearch

!!! warning
    RBD external provisionner required.

### Deployment with Helm

```sh
helm repo add incubator http://storage.googleapis.com/kubernetes-charts-incubator
helm install --name my-elastic incubator/elasticsearch --set master.persistence.storageClass=rbd,data.persistence.storageClass=rbd
```

### Show resources

```sh
root@kubemaster:~# kubectl get all --selector="app=elasticsearch" -o wide
NAME                                                   READY     STATUS    RESTARTS   AGE       IP           NODE        NOMINATED NODE
pod/my-elastic-elasticsearch-client-65888fb766-m6gfn   1/1       Running   0          15m       10.244.1.4   kubenode1   <none>
pod/my-elastic-elasticsearch-client-65888fb766-mlnxg   1/1       Running   0          15m       10.244.2.5   kubenode0   <none>
pod/my-elastic-elasticsearch-data-0                    1/1       Running   0          15m       10.244.3.7   kubenode2   <none>
pod/my-elastic-elasticsearch-data-1                    1/1       Running   0          13m       10.244.4.5   kubenode3   <none>
pod/my-elastic-elasticsearch-master-0                  1/1       Running   0          15m       10.244.4.4   kubenode3   <none>
pod/my-elastic-elasticsearch-master-1                  1/1       Running   0          13m       10.244.2.6   kubenode0   <none>
pod/my-elastic-elasticsearch-master-2                  1/1       Running   0          12m       10.244.3.8   kubenode2   <none>

NAME                                         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE       SELECTOR
service/my-elastic-elasticsearch-client      ClusterIP   10.105.190.185   <none>        9200/TCP   15m       app=elasticsearch,component=client,release=my-elastic
service/my-elastic-elasticsearch-discovery   ClusterIP   None             <none>        9300/TCP   15m       app=elasticsearch,component=master,release=my-elastic

NAME                                              DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE       CONTAINERS      IMAGES                        SELECTOR
deployment.apps/my-elastic-elasticsearch-client   2         2         2            2           15m       elasticsearch   docker.elastic.co/elasticsearch/elasticsearch-oss:6.3.1   app=elasticsearch,component=client,release=my-elastic

NAME                                                         DESIRED   CURRENT   READY     AGE       CONTAINERS      IMAGES                    SELECTOR
replicaset.apps/my-elastic-elasticsearch-client-65888fb766   2         2         2         15m       elasticsearch   docker.elastic.co/elasticsearch/elasticsearch-oss:6.3.1   app=elasticsearch,component=client,pod-template-hash=2144496322,release=my-elastic

NAME                                               DESIRED   CURRENT   AGE       CONTAINERS      IMAGES
statefulset.apps/my-elastic-elasticsearch-data     2         2         15m       elasticsearch   docker.elastic.co/elasticsearch/elasticsearch-oss:6.3.1
statefulset.apps/my-elastic-elasticsearch-master   3         3         15m       elasticsearch   docker.elastic.co/elasticsearch/elasticsearch-oss:6.3.1
```

### Show persistent volume claims

```sh
kubectl get pvc
NAME                                     STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
data-my-elastic-elasticsearch-data-0     Bound     pvc-abcec776-9973-11e8-aa19-00163e9e0001   30Gi       RWO            rbd       15m
data-my-elastic-elasticsearch-data-1     Bound     pvc-4f439705-9974-11e8-aa19-00163e9e0001   30Gi       RWO            rbd       11m
data-my-elastic-elasticsearch-master-0   Bound     pvc-abd33b2d-9973-11e8-aa19-00163e9e0001   4Gi        RWO            rbd       15m
data-my-elastic-elasticsearch-master-1   Bound     pvc-4d9d94ab-9974-11e8-aa19-00163e9e0001   4Gi        RWO            rbd       11m
data-my-elastic-elasticsearch-master-2   Bound     pvc-5f563830-9974-11e8-aa19-00163e9e0001   4Gi        RWO            rbd       10m
```

### Check the service

```sh
root@kubemaster:~# kubectl get svc --selector="app=elasticsearch"
NAME                                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
my-elastic-elasticsearch-client      ClusterIP   10.106.0.219   <none>        9200/TCP   24m
my-elastic-elasticsearch-discovery   ClusterIP   None           <none>        9300/TCP   24m
root@kubemaster:~# curl http://10.106.0.219:9200
{
  "name" : "my-elastic-elasticsearch-client-65888fb766-n8q4h",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "gRgMJyyXTCmzdFeHjfswNg",
  "version" : {
    "number" : "6.3.1",
    "build_flavor" : "oss",
    "build_type" : "tar",
    "build_hash" : "eb782d0",
    "build_date" : "2018-06-29T21:59:26.107521Z",
    "build_snapshot" : false,
    "lucene_version" : "7.3.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
```

!!! Note
    Address `10.106.0.219` is only available from kubemaster. We will see bellow with kibana how to expose service outside the kubernetes cluster in order to access the web app with a browser.

## Kibana

### Deployment with Helm

```sh
helm install stable/kibana --name my-release --set env.ELASTICSEARCH_URL=http://my-elastic-elasticsearch-client:9200
```

### Expose the Web UI

Create an external access with a *NodePort* service. Create a file `external-kibana.yaml` :

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: kibana
  name: external-kibana
spec:
  type: NodePort
  ports:
    - port: 5601
      targetPort: 5601
      protocol: TCP
  selector:
    app: kibana
```

```sh
kubectl create -f external-kibana.yaml
service/external-kibana created
root@kubemaster:~# kubectl get svc --selector="app=kibana"
NAME                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
external-kibana     NodePort    10.98.181.37    <none>        5601:31745/TCP   23s
my-release-kibana   ClusterIP   10.111.193.23   <none>        443/TCP          46s
```

### Check the service

```sh
root@kubemaster:~# curl http://10.158.0.1:31745
<script>var hashRoute = '/app/kibana';
var defaultRoute = '/app/kibana';

var hash = window.location.hash;
if (hash.length) {
  window.location = hashRoute + hash;
} else {
  window.location = defaultRoute;
}</script>
```

!!! Note
    Here `10.158.0.1` is the kubemaster IP (a g5ksubnet IP : routable through Grid'5000).

Visit [http://10.158.0.1:31745](http://10.158.0.1:31745)

![Kibana Screenshot](img/kibana_screenshot.png)
