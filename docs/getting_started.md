## Configuration

```sh
cp xp.conf.template xp.conf
```

!!! tip "`xp.conf` file"
    * To customize the cluster size (default : 3), add :

    ```
    clusterSize     10 # 1 master, 9 nodes
    ```

    * To select specific OAR resources, add :

    ```
    nodeSelection   "{cluster IN ('parasilo','paravance')}"
    ```

## Deployment (~4mn)

```sh
source setup_env.sh
rake run
#...
|-- run : 00h 03m 52s
|   |-- grid5000:jobs -> 00h 00m 03s
|   |-- vm:wait -> 00h 00m 00s
|   |-- kube:initMaster -> 00h 00m 54s
|   |-- kube:waitForMaster -> 00h 00m 22s
|   |-- kube:deployFlannel -> 00h 00m 01s
|   |-- kube:waitForFlannel -> 00h 00m 23s
|   |-- kube:joinNodes -> 00h 00m 03s
|   |-- kube:deployDashboard -> 00h 00m 01s
|   |-- helm:init -> 00h 00m 04s
|   |-- helm:waitForTiller -> 00h 00m 28s
|   |-- kube:deployMonitoring -> 00h 01m 29s
|   |-- kube:startProxy -> 00h 00m 00s
|   |-- kube:showServices -> 00h 00m 00s
```

## Available services

```sh
rake kube:showServices
**** Kubernetes Dashboard
http://10.158.0.1:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login
**** Grafana
http://10.158.0.1:8001/api/v1/namespaces/monitoring/services/kube-prometheus-grafana:80/proxy/
```

!!! Tip
    Use [Grid'5000 VPN](https://www.grid5000.fr/mediawiki/index.php/VPN) or [socks proxy](https://www.grid5000.fr/mediawiki/index.php/SSH#Using_OpenSSH_SOCKS_proxy) for external access.

### Kubernetes dashboard

![Dashboard Screenshot](img/dashboard_screenshot.png)

### Grafana

![Grafana Screenshot](img/grafana_screenshot.png)


## Open a shell on the master node

```sh
rake shell
```

## Inspect deployment

### Cluster informations

```sh
root@kubemaster:~# kubectl cluster-info
Kubernetes master is running at https://10.158.0.1:6443
KubeDNS is running at https://10.158.0.1:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

### Components statuses

```sh
root@kubemaster:~# kubectl get cs
NAME                 STATUS    MESSAGE              ERROR
scheduler            Healthy   ok
controller-manager   Healthy   ok
etcd-0               Healthy   {"health": "true"}
```

### Cluster nodes

```sh
root@kubemaster:~# kubectl get nodes -o wide
NAME         STATUS    ROLES     AGE       VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE           KERNEL-VERSION      CONTAINER-RUNTIME
kubemaster   Ready     master    23m       v1.11.1   10.158.0.1    <none>        Ubuntu 18.04 LTS   4.15.0-20-generic   docker://17.12.1-ce
kubenode0    Ready     <none>    22m       v1.11.1   10.158.0.2    <none>        Ubuntu 18.04 LTS   4.15.0-20-generic   docker://17.12.1-ce
kubenode1    Ready     <none>    22m       v1.11.1   10.158.0.3    <none>        Ubuntu 18.04 LTS   4.15.0-20-generic   docker://17.12.1-ce
kubenode2    Ready     <none>    22m       v1.11.1   10.158.0.4    <none>        Ubuntu 18.04 LTS   4.15.0-20-generic   docker://17.12.1-ce
kubenode3    Ready     <none>    22m       v1.11.1   10.158.0.5    <none>        Ubuntu 18.04 LTS   4.15.0-20-generic   docker://17.12.1-ce
```

### Pods

```sh
root@kubemaster:~# kubectl get pod --all-namespaces -o wide
NAMESPACE     NAME                                                   READY     STATUS    RESTARTS   AGE       IP           NODE         NOMINATED NODE
kube-system   coredns-78fcdf6894-cq28c                               1/1       Running   0          20m       10.244.0.2   kubemaster   <none>
kube-system   coredns-78fcdf6894-g6m78                               1/1       Running   0          20m       10.244.0.3   kubemaster   <none>
kube-system   etcd-kubemaster                                        1/1       Running   0          19m       10.158.0.1   kubemaster   <none>
kube-system   kube-apiserver-kubemaster                              1/1       Running   0          20m       10.158.0.1   kubemaster   <none>
kube-system   kube-controller-manager-kubemaster                     1/1       Running   0          19m       10.158.0.1   kubemaster   <none>
kube-system   kube-flannel-ds-nqndr                                  1/1       Running   0          20m       10.158.0.1   kubemaster   <none>
kube-system   kube-flannel-ds-nwwnc                                  1/1       Running   0          20m       10.158.0.3   kubenode1    <none>
kube-system   kube-flannel-ds-pdnrg                                  1/1       Running   0          20m       10.158.0.4   kubenode2    <none>
kube-system   kube-flannel-ds-s7vb9                                  1/1       Running   0          20m       10.158.0.2   kubenode0    <none>
kube-system   kube-flannel-ds-th9rz                                  1/1       Running   0          20m       10.158.0.5   kubenode3    <none>
kube-system   kube-proxy-f2hjq                                       1/1       Running   0          20m       10.158.0.4   kubenode2    <none>
kube-system   kube-proxy-qvf5d                                       1/1       Running   0          20m       10.158.0.2   kubenode0    <none>
kube-system   kube-proxy-r6qcn                                       1/1       Running   0          20m       10.158.0.5   kubenode3    <none>
kube-system   kube-proxy-rbshg                                       1/1       Running   0          20m       10.158.0.3   kubenode1    <none>
kube-system   kube-proxy-x774x                                       1/1       Running   0          20m       10.158.0.1   kubemaster   <none>
kube-system   kube-scheduler-kubemaster                              1/1       Running   0          20m       10.158.0.1   kubemaster   <none>
kube-system   kubernetes-dashboard-6948bdb78-2dc65                   1/1       Running   0          20m       10.244.0.4   kubemaster   <none>
kube-system   tiller-deploy-759cb9df9-cmjpg                          1/1       Running   0          20m       10.244.2.2   kubenode0    <none>
monitoring    alertmanager-kube-prometheus-0                         2/2       Running   0          18m       10.244.3.4   kubenode2    <none>
monitoring    kube-prometheus-exporter-kube-state-69d85f857b-rf2r5   2/2       Running   0          18m       10.244.2.4   kubenode0    <none>
monitoring    kube-prometheus-exporter-node-cfwfx                    1/1       Running   0          18m       10.158.0.5   kubenode3    <none>
monitoring    kube-prometheus-exporter-node-hm8pn                    1/1       Running   0          18m       10.158.0.2   kubenode0    <none>
monitoring    kube-prometheus-exporter-node-hqfgt                    1/1       Running   0          18m       10.158.0.3   kubenode1    <none>
monitoring    kube-prometheus-exporter-node-mqvq8                    1/1       Running   0          18m       10.158.0.1   kubemaster   <none>
monitoring    kube-prometheus-exporter-node-sfzp4                    1/1       Running   0          18m       10.158.0.4   kubenode2    <none>
monitoring    kube-prometheus-grafana-6596c455c8-b4p8g               2/2       Running   0          18m       10.244.3.5   kubenode2    <none>
monitoring    prometheus-kube-prometheus-0                           3/3       Running   1          18m       10.244.1.3   kubenode1    <none>
monitoring    prometheus-operator-858c485-g2fqs                      1/1       Running   0          19m       10.244.4.2   kubenode3    <none>
```