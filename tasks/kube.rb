namespace :kube do

  desc 'Init master'
  task :initMaster do
    ip = clusterDesc()['master']['kubemaster'] 
    logs = on(ip, :user => 'root') do
      'kubeadm token generate'
    end
    a = clusterDesc()
    a['token'] = logs[ip].chomp
    updateDesc(a.to_json)
    on(ip, :user => 'root') do
      [
        "kubeadm init --pod-network-cidr=10.244.0.0/16 --token #{a['token']}",
        "sleep 4 && mkdir .kube && cp /etc/kubernetes/admin.conf .kube/config"
      ]
    end
  end

  desc 'Wait for master'
  task :waitForMaster do
    ip = clusterDesc()['master']['kubemaster']
    numberAvailable = "0"
    until numberAvailable == "1"
      logs = on(ip, :user => 'root') do
        "kubectl get daemonset -n kube-system -o jsonpath='{.items[0].status.numberAvailable}'"
      end
      numberAvailable = logs[ip]
      sleep(2)
    end
  end

  desc 'Deploy flannel'
  task :deployFlannel do
    ip = clusterDesc()['master']['kubemaster']
    on(ip, :user => 'root') do
      "kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml"
    end
  end

  desc 'Wait fo flannel'
  task :waitForFlannel do
    ip = clusterDesc()['master']['kubemaster']
    unavailableReplicas = "1"
    until unavailableReplicas == ""
      sleep(5)
      logs = on(ip, :user => 'root') do
        "kubectl get deployment coredns -n kube-system -o jsonpath='{.status.unavailableReplicas}'"
      end
      unavailableReplicas = logs[ip]
    end
  end

  desc 'Join nodes'
  task :joinNodes do
    desc = clusterDesc()
    nodes = desc['nodes'].keys.collect { |x| desc['nodes'][x] }
    on(nodes, :user => 'root') do
      "kubeadm join --token #{desc['token']} #{desc['master']['kubemaster']}:6443 --discovery-token-unsafe-skip-ca-verification"
    end
  end

  desc 'Start proxy'
  task :startProxy do
    ip = clusterDesc()['master']['kubemaster']
    on(ip, :user => 'root') do
      'nohup kubectl proxy --address="0.0.0.0" --accept-hosts=".*" >&- 2>&- <&- &'
    end
  end

  desc 'Deploy Kubernetes dashboard'
  task :deployDashboard do
    ip = clusterDesc()['master']['kubemaster']
    repoLocalPath = Dir.pwd
    repoVmPath = repoLocalPath.sub(ENV['HOME'], "/mnt/home")
    on(ip, :user => 'root') do
      [
        "kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml",
        "kubectl create -f #{repoVmPath}/kubernetes/dashboard/admin-serviceaccount.yaml"
      ]
    end
  end

  desc 'Deploy Monitoring'
  task :deployMonitoring do
    ip = clusterDesc()['master']['kubemaster']
    on(ip, :user => 'root') do
      [
        "helm repo add coreos https://s3-eu-west-1.amazonaws.com/coreos-charts/stable/",
        "helm install coreos/prometheus-operator --name prometheus-operator --namespace monitoring",
        "helm install coreos/kube-prometheus --name kube-prometheus --set global.rbacEnable=true --namespace monitoring",
        %{kubectl set env deployment/kube-prometheus-grafana GF_SERVER_ROOT_URL="http://#{ip}:8001/api/v1/namespaces/monitoring/services/kube-prometheus-grafana:80/proxy/" -n monitoring}
      ]
    end
  end

  desc 'Show services URL'
  task :showServices do
    ip = clusterDesc()['master']['kubemaster']
    puts "**** Kubernetes Dashboard"
    puts "http://#{ip}:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login"
    puts "**** Grafana"
    puts "http://#{ip}:8001/api/v1/namespaces/monitoring/services/kube-prometheus-grafana:80/proxy/"
  end

end
