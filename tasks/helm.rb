namespace :helm do

  desc 'Init Helm'
  task :init do
    ip = clusterDesc()['master']['kubemaster']
    on(ip, :user => 'root') do
      [
        'helm init',
        'kubectl create serviceaccount --namespace kube-system tiller',
        'kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller',
        %{kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'},
        'helm init --service-account tiller --upgrade'
      ]
    end
  end

  desc 'Wait for tiller'
  task :waitForTiller do
    ip = clusterDesc()['master']['kubemaster']
    unavailableReplicas = "1"
    until unavailableReplicas == ""
      sleep(2)
      logs = on(ip, :user => 'root') do
        "kubectl get deployment tiller-deploy -n kube-system -o jsonpath='{.status.unavailableReplicas}'"
      end
      unavailableReplicas = logs[ip]
    end
  end

end