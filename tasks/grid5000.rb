# XP5K job
#
resources = [] << %{slash_22=1+#{XP5K::Config[:nodeSelection]}/nodes=#{XP5K::Config[:clusterSize]},walltime=#{XP5K::Config[:walltime]}}

roles = [
  XP5K::Role.new({ name: 'kubemaster_host', size: 1 }),
  XP5K::Role.new({ name: 'kubenode_host', size: (XP5K::Config[:clusterSize] - 1) })
]

@job_def = {
  resources:  resources,
  site:       XP5K::Config[:site],
  name:       XP5K::Config[:jobname],
  roles:      roles,
  command:    Dir.pwd + "/oar_deploy_k8s.rb #{Dir.pwd}"
}

@job_def[:reservation] = XP5K::Config[:reservation] unless XP5K::Config[:reservation].nil?
@job_def[:jobid] =  XP5K::Config[:jobid] unless XP5K::Config[:jobid].nil?

xp.define_job(@job_def)


# Job management tasks
#
namespace :grid5000 do

  desc 'Submit OAR jobs'
  task :jobs do
    xp.submit
    xp.wait_for_jobs
  end

  desc 'Get OAR jobs status'
  task :status do
    xp.status
  end

  desc 'Clean all OAR jobs'
  task :clean do
    puts "Clean all Grid'5000 running jobs..."
    xp.clean
  end

end

def jobID
  xp.jobs.select { |x| x["name"] == XP5K::Config[:jobname] }.first['uid']
end