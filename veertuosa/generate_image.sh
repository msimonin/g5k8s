#!/usr/bin/env bash
#OAR -n g5K8s image generate
#OAR -l /nodes=1,walltime=0:30:00

set -e

# Activate modules.
#
. /grid5000/Modules/modules.sh


# Load Veertuosa module.
#
module load veertuosa


# Vars declaration.
#
LOCAL_PATH=$(pwd)
VM_PATH=$(echo ${LOCAL_PATH} | sed "s~$HOME~/mnt/home~")
IMAGE_FILE=veertuosa/images/g5k8s.qcow2

# Copy a basic image into veertuosa/images.
#
if [ ! -f ${IMAGE_FILE} ]; then
  cp /grid5000/images/veertuosa/ubuntu-18.04-x86_64.qcow2 ${IMAGE_FILE}
else
  echo "Image ${IMAGE_FILE} already exists ! Remove file and generate again"
  exit 1
fi


# Start the VM.
#
veertuosa_launch --name kube --image ${IMAGE_FILE} --persistent


# Install Kubernetes and configure system.
# VM will shutting down at the end.
#
ssh-vm kube sh ${VM_PATH}/veertuosa/install_k8s.sh

exit 0
