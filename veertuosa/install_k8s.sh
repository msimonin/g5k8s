#!/usr/bin/env bash
set -e

# Install needed packages
apt update && apt install docker.io vim curl gpg ceph-common -y
systemctl enable docker

# Install Kubernetes APT repo
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list

# Install Kubernetes
apt update && apt install kubeadm kubectl kubelet -y

# Disable swp
swapoff -a
sed -i '/swap/d' /etc/fstab

# Pull docker images
#kubeadm config images pull

# Install Helm
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash

# Clean APT caches and shutdown the VM
apt-get clean
shutdown now
